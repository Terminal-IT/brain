# Brain

> Help me to remember...

# Thema: Terminal-IT

> Administration auf der Komandozeile

Shellscript ![](https://icongr.am/material/bash.svg?size=80) 
Shell/Bash <img src="https://camo.githubusercontent.com/0d7b856deafbf14a94ee809c4768081e03186dc1/687474703a2f2f6a6f6e2e646568646172692e6f72672f696d616765732f6c6f676f732f626173682e706e67" height="80" width="80"/>
Terminal <img src="https://cdn0.iconfinder.com/data/icons/octicons/1024/terminal-512.png" height="80" width="80"/>
Linux <img src="https://banner2.cleanpng.com/20180705/hpy/kisspng-tux-racer-linux-computer-icons-linux-foundation-5b3eafff280586.2022127615308349431639.jpg" height="80" width="80"/>
RedHat <img src="https://img.icons8.com/ios/500/red-hat.png" height="80" width="80"/>
Fedora ![](https://icongr.am/simple/fedora.svg?size=80)

![](https://ps.w.org/optinmonster/assets/icon-256x256.png?rev=1145864)
[Popups by OptinMonster – Best WordPress Popup Plugin](https://de.wordpress.org/plugins/optinmonster/)
![](https://icongr.am/fontawesome/optin-monster.svg?size=80)

# Icons

> [Devicon Icons](https://icongr.am/devicon)

![](https://icongr.am/devicon/ceylon-plain.svg?size=80"ceylon-plain")
![](https://icongr.am/devicon/drupal-plain.svg?size=80)
![](https://icongr.am/devicon/go-plain.svg?size=80)
![](https://icongr.am/devicon/gradle-plain.svg?size=80)
![](https://icongr.am/devicon/grunt-plain.svg?size=80)
![](https://icongr.am/devicon/rails-plain.svg?size=80)

> [Entypo icons](https://icongr.am/entypo)

![](https://icongr.am/entypo/feather.svg?size=80)
![](https://icongr.am/entypo/github.svg?size=80)
![](https://icongr.am/entypo/users.svg?size=80)

> [Jam icons](https://icongr.am/jam)

![](https://icongr.am/jam/aperture.svg?size=80)
![](https://icongr.am/jam/ghost-f.svg?size=80)
![](https://icongr.am/jam/hammer-f.svg?size=80)

> [Font Awesome icons](https://icongr.am/fontawesome)

![](https://icongr.am/fontawesome/chrome.svg?size=80)
![](https://icongr.am/fontawesome/drupal.svg?size=80)
![](https://icongr.am/fontawesome/empire.svg?size=80)
![](https://icongr.am/fontawesome/first-order.svg?size=80)
![](https://icongr.am/fontawesome/gears.svg?size=80)
![](https://icongr.am/fontawesome/gg-circle.svg?size=80)
![](https://icongr.am/fontawesome/github.svg?size=80)
![](https://icongr.am/fontawesome/group.svg?size=80)
![](https://icongr.am/fontawesome/grav.svg?size=80)
![](https://icongr.am/fontawesome/linode.svg?size=80)
![](https://icongr.am/fontawesome/low-vision.svg?size=80)
![](https://icongr.am/fontawesome/rebel.svg?size=80)
![](https://icongr.am/fontawesome/road.svg?size=80)
![](https://icongr.am/fontawesome/themeisle.svg?size=80)
![](https://icongr.am/fontawesome/tripadvisor.svg?size=80)
![](https://icongr.am/fontawesome/tty.svg?size=80)
![](https://icongr.am/fontawesome/user-secret.svg?size=80)
![](https://icongr.am/fontawesome/weixin.svg?size=80)

> [Feather icons](https://icongr.am/feather)

![](https://icongr.am/feather/slack.svg?size=80)

> [Material Design icons](https://icongr.am/material)

![](https://icongr.am/material/account-child.svg?size=80)
![](https://icongr.am/material/android-auto.svg?size=80)
![](https://icongr.am/material/anvil.svg?size=80)
![](https://icongr.am/material/arch.svg?size=80)
![](https://icongr.am/material/bash.svg?size=80)
![](https://icongr.am/material/battlenet.svg?size=80)
![](https://icongr.am/material/blender-software.svg?size=80)
![](https://icongr.am/material/blur.svg?size=80)
![](https://icongr.am/material/boomerang.svg?size=80)
![](https://icongr.am/material/carrot.svg?size=80)
![](https://icongr.am/material/charity.svg?size=80)
![](https://icongr.am/material/cryengine.svg?size=80)
![](https://icongr.am/material/death-star-variant.svg?size=80)
![](https://icongr.am/material/docker.svg?size=80)
![](https://icongr.am/material/freebsd.svg?size=80)
![](https://icongr.am/material/gate-xor.svg?size=80)
![](https://icongr.am/material/gnome.svg?size=80)
![](https://icongr.am/material/home-assistant.svg?size=80)
![](https://icongr.am/material/jellyfish-outline.svg?size=80)
![](https://icongr.am/material/linux.svg?size=80)
![](https://icongr.am/material/meetup.svg?size=80)
![](https://icongr.am/material/ninja.svg?size=80)
![](https://icongr.am/material/orbit.svg?size=80)
![](https://icongr.am/material/penguin.svg?size=80)
![](https://icongr.am/material/sheep.svg?size=80)
![](https://icongr.am/material/tortoise.svg?size=80)
![](https://icongr.am/material/turtle.svg?size=80)


> [Octicons](https://icongr.am/octicons)

![](https://icongr.am/octicons/rocket.svg?size=80)
![](https://icongr.am/octicons/flame.svg?size=80)

> [Simple icons](https://icongr.am/simple)

![](https://icongr.am/simple/aurelia.svg?size=80)
![](https://icongr.am/simple/bathasu.svg?size=80)
![](https://icongr.am/simple/brave.svg?size=80)
![](https://icongr.am/simple/cloudflare.svg?size=80)
![](https://icongr.am/simple/codeship.svg?size=80)
![](https://icongr.am/simple/crunchyroll.svg?size=80)
![](https://icongr.am/simple/discord.svg?size=80)
![](https://icongr.am/simple/docker.svg?size=80)
![](https://icongr.am/simple/elasticsearch.svg?size=80)
![](https://icongr.am/simple/evernote.svg?size=80)
![](https://icongr.am/simple/expo.svg?size=80)
![](https://icongr.am/simple/fedora.svg?size=80)

# Sonstige Monster Ideen

![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRm1m_iboqtsMaxmWYtgcFvpmtJ3wHyhBS0N6avIjvILMADd3ql&usqp=CAU)
![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS3IafRW-WAXsNPlFiQ3rc_bT-ucSeNcCnwDjvghOolJ55ZxnPD&usqp=CAU)
![](https://clip.cookdiary.net/sites/default/files/wallpaper/drawn-graffiti/368878/drawn-graffiti-monster-368878-129814.jpg)
![](https://clip.cookdiary.net/sites/default/files/wallpaper/drawn-graffiti/368878/drawn-graffiti-monster-368878-52511.jpg)
![](https://clip.cookdiary.net/sites/default/files/wallpaper/drawn-graffiti/368878/drawn-graffiti-monster-368878-970998.jpg)
![](https://clip.cookdiary.net/sites/default/files/wallpaper/drawn-graffiti/368878/drawn-graffiti-monster-368878-8256468.jpg)
![](https://clip.cookdiary.net/sites/default/files/wallpaper/drawn-graffiti/368878/drawn-graffiti-monster-368878-530812.jpg)
![](https://s3.amazonaws.com/dentedreality-content/wp-content/uploads/2005/10/14184049/57918447_26fcd28fed_o-1024x697.jpg)
![](https://res.cloudinary.com/teepublic/image/private/s--sWVPEUCg--/t_Resized%20Artwork/c_fit,g_north_west,h_1054,w_1054/co_ffffff,e_outline:53/co_ffffff,e_outline:inner_fill:53/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1495247380/production/designs/1613276_1.jpg)
![](https://media.istockphoto.com/vectors/graffiti-monsters-vector-id518833874)
![](https://paintingvalley.com/download-image#drawing-graffiti-characters-7.jpg)
![](https://s3.envato.com/files/179196551/01design.jpg)
![](https://previews.123rf.com/images/martincp/martincp1508/martincp150800178/43535394-bordeaux-frankreich-24-juni-2015-waldpflanzen-monster-mit-gro%C3%9Fen-z%C3%A4hnen-graffiti-an-zaunfeld.jpg)
![](http://s5.favim.com/orig/51/graffiti-monster-Favim.com-467771.jpg)