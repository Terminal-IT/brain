# NeoVIM
## Installation

```sh
$ sudo dnf install neovim
$ sudo dnf install wget fuse fuse-libs git ack python3-pip perl-CPAN ruby ruby-devel nodejs # diese Packete wurden im Zusammenhang mit neovim und vim-plug zusätzlich benötigt
$ export EDITOR="nvim"
```
## Konfiguration
```sh
$ mkdir ~/.config/nvim
$ nvim ~/.config/nvim/init.vim
```
In der Datei `~/.config/nvim/init.vim` wird die Konfiguration von NeoVIM vorgenommen. Es gibt auch andere Möglichkeiten, dies ist der Standard. Meine Grundkonfiguration entstammt hauptsächlich dem Artikel [Learn Vim For the Last Time](https://danielmiessler.com/study/vim/).

> Ein wirklich gutes Tutorial!

```
inoremap jk <ESC>       " map the leader key (activation key for shortcuts) to the right on your right pinky
let mapleader = "'"     " type jk instead of pressing ESC, which is much easier
set hlsearch            " highlight all results    
set ignorecase          " ignore case in search    
set incsearch           " show search results as you type    
set noswapfile          " disable the swapfile    
set number              " show line numbers
syntax on               " highlight syntax
```
Wer paralell noch mit `vim` arbeitet kann die Konfiguration verlinken. 
```sh
$ ln -s .config/nvim/init.vim .vimrc
```
Und hier die Plugin-Sektion in der `~/.config/nvim/init.vim`

```
"""""""""""""""""""""""""
" Config for vim-plug (https://github.com/junegunn/vim-plug)
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/site/pack
call plug#begin('~/.local/share/nvim/site/pack')

" Make sure you use single quotes

" https://github.com/Pocco81/TrueZen.nvim
Plug 'Pocco81/TrueZen.nvim'

" https://github.com/plasticboy/vim-markdown
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

" https://github.com/iamcco/markdown-preview.nvim
" If you have nodejs and yarn
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }

" Initialize plugin system
call plug#end()
"""""""""""""""""""""""""

" for markdown-preview
let g:mkdp_browser = 'brave-browser'
```
Damit das Plugin `markdown-preview.vim` funktioniert muss in der letzten Zeile der Browser eingetragen werden. Bei mir ist das der Brave-Browser, bei euch eventuell ein anderer.

### Fehlersuche bei Neovim

Mit dem Befehl `:checkhealth` kann grundsätzlich überprüft werden, ob vim funktioniert, alle Plugins fehlerfrei funktionieren und alle Abhängigkeiten installiert sind. Wenn ein Befehl wie zum Beispiel, dass Ausführen eines Plugins nicht funktioniert, kann die Fehlerausgabe mit `:message` eingesehen werden.

## NeoVIM Plugins

### VIM-PLUG

Als Plugin-Manager nutze ich `vim-plug` [A minimalist Vim plugin manager](https://github.com/junegunn/vim-plug). Die unterschiedlichen Installtionsmöglichkeiten sind am besten der Seite zu entnehmen.
Ich habe mich mit Neovim zu folgender Variante entschieden.

```sh
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```

Das Verzeichnis für die Plugins anlegen.

```sh
$ mkdir ~/.local/share/nvim/site/pack
```

Ein Auszug der Anleitung von der Webseite des Projektes.

> Usage  
> Add a vim-plug section to your `init.vim` for Neovim.  
> Begin the section with call plug#begin() and end the section with `call plug#end()`  
> Reload `init.vim` and `:PlugInstall` to install plugins.  

Hier noch die Anleitung für Updates der Plugins.

> **PlugInstall! and PlugUpdate!**  
>The installer takes the following steps when installing/updating a plugin:  
>- git clone or git fetch from its origin  
>- Check out branch, tag, or commit and optionally git merge remote branch  
>- If the plugin was updated (or installed for the first time)  
>	- Update submodules  
>	- Execute post-update hooks  
>	
> The commands with the ! suffix ensure that all steps are run unconditionally.

### Das Plugin `markdown-preview.nvim`

Die Installationsanweisungen kann der Projektseite von [Markdown Preview for (Neo)vim](https://github.com/iamcco/markdown-preview.nvim) entnommen werden. Ich hatte allerdings ein Problem das Plugin ans laufen zu bekommen, was wahrscheinlich auf fehlenden Abhängigkeiten bei meiner minimalen Fedora Installation und meiner Unwissenheit bezüglich `python` und `pip` zurückzuführen ist.  
Mit folgenden Schritten habe ich das Plugin schließlich installiert.

HIER IST NICHT KLAR, OB WIRKLICH ALLES BENÖTIGT WIRD!

```sh
$ sudo dnf install yarnpkg
$ gem install neovim
$ gem enviroment
$ sudo npm install --global yarn
$ npm install tslib neovim log4js socket.io msgpack-lite
$ pip install wheel neovim pynvim tslib
$ sudo cpan install CPAN
$ sudo cpan Neovim::ext
$ yarn add tslib
```

Die Konfiguration des Plugins wird in der Datei `~/.local/share/nvim/site/pack/markdown-preview.nvim/plugin/mkdp.vim` vorgenommen.

Diese [Anleitung](https://github.com/iamcco/markdown-preview.nvim/issues/188#issuecomment-841356921) war dann ausschlaggebend.

> Hello everyone I had the same probelm and i was fixit follow the instructions with this answers:  
> 1. Remove NodeJS lastest and install Node.js
> 2. Remove plugin in nvim with your plugin manager, in my case is Plug Manager. First commenten the line of Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' } , next is close .vimrc or init.vim (in my case .vimrc), reopen .vimrc and write :PlugClean, and put Y to remove plugin.
> 3. Install LTS Node and NPM, in debian i was built from code, in ubuntu or another distros with sudo install nodejs npm
> 4. In terminal with sudo write sudo npm install tslib && sudo npm install -g yarn; yarn add tslib
> 5. Open your .vimrc or init.vim file and uncomment Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' } and close file
> 6. Reopen your .vimrc or init.vim and puts :PlugInstall
> 7. add in your .vimrc or init.vim the line let g:mkdp_browser = 'firefox' if you use firefox, I use firefox XD. Close your vim file configuration.
> 8. Create any markdown file to test it
>
> IMPORTANT: install tslib is very important to put woking on the plugin!

### Das Plugin [Vim Markdown](https://github.com/plasticboy/vim-markdown)

Folgende Zeilen in die Plugin-Sektion in der `init.vim` einfügen.

```
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
```

Dann in Neovim das Plugin installieren.

```
:so ~/.config/nvim/init.vim
:PluginInstall
```

Grundlegende Bedienung.

> Folding is enabled for headers by default.
>
> The following commands are useful to open and close folds:
>
>- **zr**: reduces fold level throughout the buffer
>- **zR**: opens all folds
>- **zm**: increases fold level throughout the buffer
>- **zM**: folds everything all the way
>- **za**: open a fold your cursor is on
>- **zA**: open a fold your cursor is on recursively
>- **zc**: close a fold your cursor is on
>- **zC**: close a fold your cursor is on recursively
>
>Try :help fold-expr and :help fold-commands for details.

### Das Plugin [TrueZen.nvim](https://github.com/Pocco81/TrueZen.nvim)

Folgende Zeilen in die Plugin-Sektion in der `init.vim` einfügen.

```
Plug 'Pocco81/TrueZen.nvim'
```

Dann in Neovim das Plugin installieren.

```
:so ~/.config/nvim/init.vim
:PluginInstall
```
Grundlegende Bedienung.

> Usage (commands)
> All the commands follow the *camel casing* naming convention and have the `TZ` prefix so that it's easy to remember that they are part of this plugin. These are all of them:
>
> **Default**
>- `:TZMinimalist` toggles Minimalist mode. Activates/deactivates NeoVim's UI components from the left, bottom and top of NeoVim on all the buffers you enter in the current session.
>- `:TZFocus` toggles Focus mode. Maximizes/minimizes the current window.
>- `:TZAtaraxis` toggles Ataraxis mode. Ataraxis is kind of a "super extension" of Minimalist mode that uses it for deactivating UI components, however, it also provides padding to all buffers in the current session + makes use of the different integrations. Furthermore, you could also set values for the padding of the left (`l`), right (`r`), top (`t`), and bottom (`b`) of the Ataraxis instance you are about to spawn. This values are optional and when given their equivalent from the config is ignored. They should be separated by spaces and the format they should have is: `<(l)eft/(r)ight>/(t)op/(b)ottom<number_of_cells>`. 
> 
> Here is an example:
> ```:TZAtaraxis l10 r10 t3 b1```
>
> Note: it's not mandatory to give all four of them.
>
> **UI Elements**
>- `:TZBottom` toggles the bottom part of NeoVim's UI. It toggles: laststatus, ruler, showmode, showcmd, and cmdheight.
>- `:TZTop` toggles the top part of NeoVim's UI. It toggles: tabline.
>- `:TZLeft` toggles the left part of NeoVim's UI. It toggles: numbers, relative numbers, and signcolumn.
>
>**On/Off**
>- `:TZAtaraxisOn` turns on Ataraxis mode.
>- `:TZAtaraxisOff` turns off Ataraxis mode.
>- `:TZMinimalistOn` turns on Minimalist mode.
>- `:TZMinimalistOff` turns off Minimalist mode.
>- `:TZFocusOn` turns on Focus mode
>- `:TZFocusOff` turns off Focus mode
>
> The following commands are enabled is the ui_elements_commands setting is set to true as well:
>
>- `:TZBottomOn` show bottom UI parts.
>- `:TZBottomOff` hide bottom UI parts.
>- `:TZTopOn` show top UI parts.
>- `:TZTopOff` hide top UI parts.
>- `:TZLeftOn` show left UI parts.
>- `:TZLeftOff` hide left UI parts

## Weitere Plugins und Konfigurationen

- [Vim/Neovim plugins for writers](https://alpha2phi.medium.com/vim-neovim-plugins-for-writing-d18414c7b21d)
- Evtl.: [neovim Teil 4 – Alle Plugins meiner Beispiel-Config erklärt](https://schimana.net/2021/neovim-teil-4-alle-plugins-meiner-beispiel-config-erklaert/)
